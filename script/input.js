// map button names to physical buttons here
// This is the only part of this file that should be changed
var buttonMap = {"jump":"a", "run":"x"};
var analogMap   = {"moveX":["leftStick", "x"], "moveY":["leftStick", "y"]};

// create input object and helper maps
var input = new PxGamepad();

var wasPressed = {'a':0, 'b':0, 'x':0, 'y':0, 'leftTop':0, 'rightTop':0, 'leftTrigger':0,
                  'rightTrigger':0, 'select':0, 'start':0, 'leftStick':0, 'rightStick':0,
                  'dpadUp':0, 'dpadDown':0, 'dpadLeft':0, 'dpadRight':0};

var downMap  = {'a':0, 'b':0, 'x':0, 'y':0, 'leftTop':0, 'rightTop':0, 'leftTrigger':0,
             'rightTrigger':0, 'select':0, 'start':0, 'leftStick':0, 'rightStick':0,
             'dpadUp':0, 'dpadDown':0, 'dpadLeft':0, 'dpadRight':0};

var upMap    = {'a':0, 'b':0, 'x':0, 'y':0, 'leftTop':0, 'rightTop':0, 'leftTrigger':0,
             'rightTrigger':0, 'select':0, 'start':0, 'leftStick':0, 'rightStick':0,
             'dpadUp':0, 'dpadDown':0, 'dpadLeft':0, 'dpadRight':0};

// button was pressed down this frame
function down(buttonName){
	return downMap[ buttonMap[buttonName] ];
}

// button was released up this frame
function up(buttonName){
	return upMap[ buttonMap[buttonName] ];
}

// button is currently down
function pressed(buttonName){
	return input.buttons[ buttonMap[buttonName] ];
}

// map analog name to phisical analog stick to get analog input
function analog(analogName){
	
	var analogData = analogMap[analogName];
	if(analogData[0] === "leftStick"){

		// Check if left stick is not being moved
		if(Math.abs(input.leftStick['x']) + Math.abs(input.leftStick['y']) < 0.3)
			return 0;

		return input.leftStick[analogData[1]];
	}
	else if(analogData[0] === "rightStick"){
		
		// Check if right stick is not being moved
		if(Math.abs(input.rightStick['x']) + Math.abs(input.rightStick['y']) < 0.3)
			return 0;
		
		return input.rightStick[analogData[1]];
	}
	else if(analogData[0] === "dpad"){
		return input.dpad[analogData[1]];
	}
}

// call once per update cycle
function updateInput(){
	
	// call update to external library
	input.update();

	// update helper maps
	for(name of input.buttonNames)
	{
		downMap[name] = !wasPressed[name] && input.buttons[name];
		upMap[name] =  wasPressed[name] && !input.buttons[name];
		wasPressed[name] = input.buttons[name];
	}

}