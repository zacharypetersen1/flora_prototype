var objectList = [];
var solidObjects = [];

function renderObjects(){
	for(object of objectList){
		object.render();
	}
}

function updateObjects(){
	for(object of objectList){
		object.update();
	}
}


// root class for an object
function GameObject(setX, setY, setW, setH){
	this.x = setX;
	this.y = setY;
	this.w = setW;
	this.h = setH;
	this.prevX = this.x;
	this.prevY = this.y;
	this.speedX = 0;
	this.speedY = 0;
	this.update = function(){

	}
	this.render = function(){
		console.error("please overwrite the render function.");
	}
}


// creates a brick using GameObject as base
function Brick(setX, setY){
	var temp = new GameObject(setX, setY, 60, 20);
	temp.collider = new BoxCollider(temp.w, temp.h, temp);
	temp.render = function(){
		fill(200, 150, 172);
		rect(applyCamX(this.x), applyCamY(this.y), this.w, this.h);
	};
	objectList.push(temp);
	solidObjects.push(temp);
}