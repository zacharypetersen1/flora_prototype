

function Player(setX, setY){
	
	var temp = new GameObject(setX, setY, 30, 60);
	temp.accel = 1;
	temp.dccel = 1;
	temp.speedCap = 9;
	temp.collider = new BoxCollider(temp.w, temp.h, temp);


	temp.update = function(){
		
		/*playerX += analog("moveX") * p_speedCap;
		playerY += analog("moveY") * p_speedCap;
		*/

		var targetSpeedX = this.speedCap * analog("moveX");
		if(this.speedX != targetSpeedX){
			this.speedX += this.accel * Math.sign(targetSpeedX - this.speedX);
		}

		var targetSpeedY = this.speedCap * analog("moveY");
		if(this.speedY != targetSpeedY){
			this.speedY += this.accel * Math.sign(targetSpeedY - this.speedY);
		}

		this.x += this.speedX;
		this.y += this.speedY;

		this.checkCollision();

		// lock camera onto player
		cameraX = this.x;
		cameraY = this.y;

		this.prevX = this.x;
		this.prevY = this.y;
	}


	temp.checkCollision = function(){
		for(solidObj of solidObjects){
			if(this.collider.checkCollision(solidObj.collider)){
				this.collider.resolveSolidCollision(solidObj.collider);
			}
		}
	}

	temp.render = function(){
		rectMode(CENTER);
		fill(100, 200, 100);
		rect(applyCamX(this.x), applyCamY(this.y), this.w, this.h);
	}

	objectList.push(temp);
}