

// point 1 is bottom left, point 2 is top right
function StaticBoxCollider(x1, y1, x2, y2){
	this.static = true;
	this.x1 = x1;
	this.y1 = y1;
	this.x2 = x2;
	this.y2 = y2;

	this.getX1 = function(){
		return this.x1
	}
	this.getY1 = function(){
		return this.y1;
	}
	this.getX2 = function(){
		return this.x2;
	}
	this.getY2 = function(){
		return this.y2;
	}

	/*this.checkCollision = function(collider){
		return !(	this.getX1() > collider.getX2() 
				|| this.getX2() < collider.getX1() 
				|| this.getY1() > collider.getY2() 
				|| this.getY2() < collider.getY1()
			  );
	}*/
}





function BoxCollider(w, h, obj){
	this.static = false;
	this.w = w;
	this.h = h;
	this.obj = obj;

	this.getX1 = function(){
		return this.obj.x - this.w/2;
	}
	this.getY1 = function(){
		return this.obj.y - this.h/2;
	}
	this.getX2 = function(){
		return this.obj.x + this.w/2;
	}
	this.getY2 = function(){
		return this.obj.y + this.h/2;
	}

	this.colliderAtLoc = function(x, y){
		return new StaticBoxCollider(x-this.w/2, y-this.h/2, x+this.w/2, y+this.h/2);
	}

	this.checkCollision = function(collider){
		return !(	this.getX1() > collider.getX2() 
				|| this.getX2() < collider.getX1() 
				|| this.getY1() > collider.getY2() 
				|| this.getY2() < collider.getY1()
			  );
	}

	this.resolveSolidCollision = function(collider){
		var pastCollider = this.colliderAtLoc(this.obj.prevX, this.obj.prevY);
		var collNum = pastCollider.getX1() - collider.getX2();
		if(collNum >= 0){
			this.obj.x = collider.getX2() + this.obj.w/2;
			this.obj.speedX = 0;
		}
		collNum = pastCollider.getX2() - collider.getX1();
		if(collNum <= 0){
			this.obj.x = collider.getX1() - this.obj.w/2;
			this.obj.speedX = 0;
		}
		collNum = pastCollider.getY1() - collider.getY2();
		if(collNum >= 0){
			this.obj.y = collider.getY2() + this.obj.h/2;
			this.obj.speedY = 0;
		}
		collNum = pastCollider.getY2() - collider.getY1();
		if(collNum <= 0){
			this.obj.y = collider.getY1() - this.obj.h/2;
			this.obj.speedY = 0;
		}
	}
}